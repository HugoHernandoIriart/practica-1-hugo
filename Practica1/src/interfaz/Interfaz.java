package interfaz;

import dominio.*;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;

public class Interfaz{
	private static String HELP_TEXT = "Texto de ayuda";
	private static String NOMBRE_FICHERO = "catalogoDeMotos.txt";

	public static void procesarPeticion(String sentencia){
		String[] args = sentencia.split(" ");
		Catalogo catalogo = inicializarCatalogo(NOMBRE_FICHERO);
		if(args[0].equals("help")){
			System.out.println(HELP_TEXT);
		} else if (args[0].equals("list")){
			if(catalogo.toString().equals("")){
				System.out.println("No hay ninguna marca en el catalogo");
			} else {
				System.out.println(catalogo);
			}
		} else if (args[0].equals("add")){
			Marca marca = new Marca(args[1], args[2]);
			catalogo.annadirMarca(marca);
			inicializarFichero(catalogo);
		}
	}

	private static void inicializarFichero(Catalogo catalogo){
		try{
			FileWriter fw = new Filewriter(NOMBRE_FICHERO);
			fw.write(catalogo.toString());
			fw.close();
		} catch (Exception e){
			System.out.println(e);

		}
	}


	private static Catalogo inicializarCatalogo(String nombreFichero){
		Catalogo catalogo = new Catalogo();
		try{
			File file = new File(nombreFichero);
			Scanner sc = new Scanner(file);
			while(sc.hasNext()){
				String marca = sc.next();
				String modelo = sc.next();
				Marca marca = new Marca(marca, modelo);
				catalogo.annadirMarca(marca);
			}
			sc.close();
		} catch (FileNotFoundException e){
			inicializarFichero(catalogo);
		} catch (Exception e){
			System.out.println(e);
		}
		return catalogo;
	}
}
			     	


