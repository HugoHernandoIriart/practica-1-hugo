package dominio;

import java.util.Arraylist;

public class Catalogo{
	private ArrayList<Marca> coleccionMarcas = new ArrayList<>();

	public void annadirMarca(Marca marca){
		coleccionMarcas.add(marca);
	}

	public String toStrint(){
		StringBuilder datos = new StringBuilder();
		for (Marca marca : coleccionesMarcas){
			datos.append(marca);
		}
		return datos.toString();
	}
}
