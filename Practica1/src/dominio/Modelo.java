package dominio;

public class moto{
	private String marca;
	private String modelo;

	public moto(){
		marca = "";
		modelo = "";
	}
	
	public moto(String marca, String modelo){
		this.marca = marca;
		this.modelo = modelo;
	}
	
	public String getMarca(){
		return marca;
	}

	public void setMarca(String marca){
		this.marca = marca;
	}

	public String getModelo(){
		return modelo;
	}

	public void setModelo(String modelo){
		this.modelo = modelo;
	}

	public String toString(){
		return marca + " " + modelo + "\n";
	}
}
